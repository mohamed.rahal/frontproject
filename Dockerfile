# stage 1
FROM node:16.20.2 AS builder
WORKDIR /app
COPY . .
COPY package.json package-lock.json ./
RUN npm install
RUN npm run build --prod
# stage 2
FROM nginx:alpine
COPY nginx.conf  /etc/nginx/conf.d/default.conf
COPY --from=builder /app/dist/summer-workshop-angular /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]